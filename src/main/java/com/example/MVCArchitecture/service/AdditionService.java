package com.example.MVCArchitecture.service;

import com.example.MVCArchitecture.model.Addition;
import org.springframework.stereotype.Service;

@Service
public class AdditionService {
    public int addition(Addition additionObject){
        int result = additionObject.getFirstNumber()+ additionObject.getSecondNumber();
        additionObject.setResult(result);
        return additionObject.getResult();
    }
}
