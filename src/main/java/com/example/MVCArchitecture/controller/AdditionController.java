package com.example.MVCArchitecture.controller;

import com.example.MVCArchitecture.model.Addition;
import com.example.MVCArchitecture.service.AdditionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdditionController {
    @Autowired
    AdditionService additionServiceObject;

    @GetMapping("/addition")
    public String addition(){
        Addition additionObject = new Addition(2,3);
       int result = additionServiceObject.addition(additionObject);
        return "addition is: "+result;
    }
}
