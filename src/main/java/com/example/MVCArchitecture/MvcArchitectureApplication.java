package com.example.MVCArchitecture;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class MvcArchitectureApplication {

	public static void main(String[] args) {

		SpringApplication.run(MvcArchitectureApplication.class, args);
	}


}
